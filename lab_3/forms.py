from lab_1.models import Friend
from django import forms
class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name','npm','DOB']

    error_messages = {
		'required' : 'Please Type'
	}

    input_attrs = [
		{'type' : 'text',
		'placeholder' : 'Nama Kamu'},
        {'type' : 'text',
		'placeholder' : 'NPM Kamu <3'},
        {'type' : 'date',
		'placeholder' : 'DOB Kamuh <3'}
    ]
    name = forms.CharField(label='', required=False, max_length=27, widget=forms.TextInput(attrs=input_attrs[0]))
    npm = forms.CharField(label='', required=False, max_length=27, widget=forms.TextInput(attrs=input_attrs[1]))
    DOB = forms.DateField(label='', required=False, widget=forms.DateInput(attrs=input_attrs[2]))
