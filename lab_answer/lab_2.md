# Jawaban Lab 2
### 1. Apa perbedaan JSON dengan XML?
Perbedaan JSON dan XML adalah bahwa XML merupakan sebuah *markup language*, sedangkan JSON merupakan sebuah format untuk menotasi sebuah objek. XML terstruktur seperti sebuah *tree* yang dimulai dari root, branch, hingga leaves, sementara  JSON terstruktur seperti sebuah *map* yang berisi pasangan *key* dan *value* 

### 2 Apakah perbedaan antara HTML dan XML?
HTML digunakan untuk menampilkan data dan membuat struktur sebuah *webpage* sementara XML digunakan untuk menyimpan dan mentransfer data.