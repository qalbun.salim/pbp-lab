from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
    
    error_messages = {
		'required' : 'Please Type'
	}

    input_attrs = [
		{'type' : 'text',
		'placeholder' : 'Dari Siapa'},
        {'type' : 'text',
		'placeholder' : 'Untuk Siapa'},
        {'type' : 'text',
		'placeholder' : 'Subject Note'},
        {'type' : 'textarea',
		'placeholder' : 'Pesan'}
    ]
    From =  forms.CharField(label='', required=False, max_length=27, widget=forms.TextInput(attrs=input_attrs[0]))
    to =  forms.CharField(label='', required=False, max_length=27, widget=forms.TextInput(attrs=input_attrs[1]))
    title =  forms.CharField(label='', required=False, max_length=27, widget=forms.TextInput(attrs=input_attrs[2]))
    message =  forms.Textarea(attrs=input_attrs[3])

    
