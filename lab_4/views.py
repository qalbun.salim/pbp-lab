from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_2.models import Note
from lab_4.forms import * 
from django.core import serializers
from django.http.response import HttpResponse

# Create your views here.
def index(request):
    mynotes = Note.objects.all().values()
    notes = {'notes' : mynotes}
    return render(request, 'index_lab4.html', notes)

# @login_required(login_url='/admin/login/')
def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4')
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    mynotes = Note.objects.all().values()
    notes = {'notes' : mynotes}
    return render(request, 'lab4_note_list.html', notes)


