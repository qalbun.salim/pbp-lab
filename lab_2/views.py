from django.shortcuts import render
from .models import Note
from django.core import serializers
from django.http.response import HttpResponse

# Create your views here.
def index(request):
    mynotes = Note.objects.all().values()
    notes = {'notes' : mynotes}
    return render(request, 'lab2.html', notes)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")


